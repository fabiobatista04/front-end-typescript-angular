class CarRental {
    private person: Person;
    private vehicle: Vehicle;
    private rentalPrice: number;
    private rentalDate: Date;
    private devolutionDate: Date;
    private dailyPrice: number;

    constructor( person: Person, vehicle: Vehicle, rentalPrice: number,
                 rentalDate: Date, devolutionDate: Date, dailyPrice: number ){

        this.person = person;
        this.vehicle = vehicle;
        this.rentalPrice = rentalPrice;
        this.rentalDate = rentalDate;
        this.devolutionDate = devolutionDate;
        this.dailyPrice = dailyPrice;        
    }    
}