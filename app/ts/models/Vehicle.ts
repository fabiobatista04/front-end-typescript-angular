class Vehicle {
    constructor( 
        private _name: string, 
        private _model: string, 
        private _manufacturer: string, 
        private _costCredits: string,
        private _length: string, 
        private _maxAtmosphereSpeed: string,
        private _crew: string, 
        private _passengers: string,
        private _cargoCapacity: string, 
        private _consumables: string,
        private _vehicleClass: string, 
        private _pilots: string[],
        private _films: string[], 
        private _created: String,
        private _edited: String, 
        private _url: String
    ){}
    get name(): string {
        return this._name;
    }
    public getModel(): string {
        return this._model;
    }

    getManufacturer(): String {
        return this._manufacturer;
    }
    set name(name: string){
        this._name = name;
    }
    public setModel(model: string): void {
        this._model = model;
    }
    setManufacturer(manufacturer: string): void {
        this._manufacturer = manufacturer;
    }

}

interface seila{
    name: string
    sobe?: string 
    [num: number]: string
}
let a: seila;

a = {
    name:"fs",
    sobe:"sef",
    1:"sdf"
}