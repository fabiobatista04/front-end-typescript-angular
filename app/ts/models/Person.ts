class Person{
    private  name: string;
    private  cpf: string;
    private  birthDate: Date;
    private  age: number;
    private  cnh: string;
    private  isRentedCar: boolean;

    constructor(name: string, cpf: string, birthDate: Date, age: number, cnh: string, isRentedCar: boolean,){
            this.name = name;
            this.cpf = cpf;
            this.birthDate = birthDate;
            this.age = age;
            this.cnh = cnh;
            this.isRentedCar = isRentedCar;
    }

}