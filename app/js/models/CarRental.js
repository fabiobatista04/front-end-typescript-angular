"use strict";
class CarRental {
    constructor(person, vehicle, rentalPrice, rentalDate, devolutionDate, dailyPrice) {
        this.person = person;
        this.vehicle = vehicle;
        this.rentalPrice = rentalPrice;
        this.rentalDate = rentalDate;
        this.devolutionDate = devolutionDate;
        this.dailyPrice = dailyPrice;
    }
}
