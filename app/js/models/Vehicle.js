"use strict";
class Vehicle {
    constructor(_name, _model, _manufacturer, _costCredits, _length, _maxAtmosphereSpeed, _crew, _passengers, _cargoCapacity, _consumables, _vehicleClass, _pilots, _films, _created, _edited, _url) {
        this._name = _name;
        this._model = _model;
        this._manufacturer = _manufacturer;
        this._costCredits = _costCredits;
        this._length = _length;
        this._maxAtmosphereSpeed = _maxAtmosphereSpeed;
        this._crew = _crew;
        this._passengers = _passengers;
        this._cargoCapacity = _cargoCapacity;
        this._consumables = _consumables;
        this._vehicleClass = _vehicleClass;
        this._pilots = _pilots;
        this._films = _films;
        this._created = _created;
        this._edited = _edited;
        this._url = _url;
    }
    get name() {
        return this._name;
    }
    getModel() {
        return this._model;
    }
    getManufacturer() {
        return this._manufacturer;
    }
    set name(name) {
        this._name = name;
    }
    setModel(model) {
        this._model = model;
    }
    setManufacturer(manufacturer) {
        this._manufacturer = manufacturer;
    }
}
let a;
a = {
    name: "fs",
    sobe: "sef",
    1: "sdf"
};
