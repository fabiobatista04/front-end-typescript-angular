"use strict";
class Person {
    constructor(name, cpf, birthDate, age, cnh, isRentedCar) {
        this.name = name;
        this.cpf = cpf;
        this.birthDate = birthDate;
        this.age = age;
        this.cnh = cnh;
        this.isRentedCar = isRentedCar;
    }
}
